﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public float velocidad = 5;
    public float fuerzaSalto = 12;
    public float gravedad = 10;
    public Transform groundCheck;
    public Transform roofCheck;

    Rigidbody2D _rigidbody;
    SpriteRenderer spriteRenderer;
    Animator animator;
    float velocidadVertical;
    bool estoyEnElPiso;
    bool tocoTecho;
    private Vector3 movimiento;

    float h;
    bool presionoSalto;

    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        animator = GetComponentInChildren<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        LeerInput();

        ManejarAnimacion();
    }

    private void FixedUpdate()
    {
        estoyEnElPiso = ChequearPiso();
        tocoTecho = ChequearTecho();
        //leemos input del teclado
        //vale 1 si presionas derecha
        //vale 0 si no presionas nada
        //vale -1 si presionas izquierda
        MovimientoHorizontal();
        MovimientoVertical();
        Saltar();

        movimiento = movimiento + new Vector3(0, velocidadVertical, 0);
        movimiento = movimiento * Time.deltaTime;
        _rigidbody.MovePosition(transform.position + movimiento);

    }

    void MovimientoHorizontal() {
        movimiento = new Vector3(h * velocidad, 0, 0);
    }

    void MovimientoVertical() {
        if (tocoTecho)
        {
            velocidadVertical = 0;
        }
        if (estoyEnElPiso)
        {
            velocidadVertical = 0;
        }
        else
        {
            velocidadVertical = velocidadVertical - (gravedad * Time.deltaTime);
        }
    }

    bool ChequearPiso() {
        RaycastHit2D hit = Physics2D.Linecast(transform.position, groundCheck.position);
        //estoy en el aire
        if (hit.collider == null)
        {
            Debug.DrawLine(transform.position, groundCheck.position, Color.red,0,false);
            return false;
        }
        else
        {//estoy en el piso
            Debug.DrawLine(transform.position, groundCheck.position, Color.green,0,false);
            return true;
        }

    }

    bool ChequearTecho()
    {
        RaycastHit2D hit = Physics2D.Linecast(transform.position, roofCheck.position);
        
        if (hit.collider == null)
        {
            Debug.DrawLine(transform.position, roofCheck.position, Color.red, 0, false);
            return false;
        }
        else
        {
            Debug.DrawLine(transform.position, roofCheck.position, Color.green, 0, false);
            return true;
        }
    }

    void Saltar() {
        if (estoyEnElPiso)
        {
            if (presionoSalto)
            {
                velocidadVertical = fuerzaSalto;
                presionoSalto = false;
            }
        }
    }

    void LeerInput()
    {
        h = Input.GetAxis("Horizontal");
        if (Input.GetButtonDown("Jump") && estoyEnElPiso)
        {
            presionoSalto = true;
        }
        

    }

    void ManejarAnimacion() {
        if (h > 0)
        {
            //derecha
            spriteRenderer.flipX = false;
        }
        if (h < 0)
        {
            //izquierda
            spriteRenderer.flipX = true;
        }

        animator.SetFloat("horizontalSpeed", Mathf.Abs(h));
        animator.SetFloat("verticalSpeed", velocidadVertical);
        animator.SetBool("isGrounded", estoyEnElPiso);
    }
}
